import React from "react";

import { useRootStore } from "../../store";

const AddNoteForm: React.FC = () => {
  const { noteStore } = useRootStore();
  const [noteText, setNoteText] = React.useState("");

  return (
    <form onSubmit={(e) => {
        e.preventDefault();
        noteStore.addNote(noteText);
        setNoteText("");
    }}>
      <input
        onChange={(e) => setNoteText(e.target.value)}
        value={noteText}
        type="text"
        placeholder="Some text"
      />
      <button type="submit">Add note</button>
    </form>
  );
};

export default AddNoteForm;
