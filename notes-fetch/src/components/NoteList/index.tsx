import React from "react";
import { Observer } from "mobx-react-lite";

import { useRootStore } from "../../store";

const NoteList: React.FC = () => {
  const { noteStore } = useRootStore();

  return (
    <Observer>
      {() => (
        <ul>
          {noteStore.notes.map((note) => (
            <li>{note}</li>
          ))}
        </ul>
      )}
    </Observer>
  );
};

export default NoteList;
