import { observable, action, makeAutoObservable } from 'mobx';

export class NoteStore {
    @observable notes: string[] = [];

    constructor() {
        makeAutoObservable(this);
    }

    @action
    saveNotes() {
        localStorage.setItem("notes", JSON.stringify(this.notes));
    }

    @action
    loadNotes() {
        this.notes = JSON.parse(localStorage.getItem("notes") || "[]");
    }

    @action
    addNote(text: string) {
        // alert(JSON.stringify(this.notes));
        this.notes.push(text);
    }

    @action
    clear() {
        this.notes = [];
        localStorage.setItem("notes", "[]");
    }
}