import React from "react";
import "./App.css";

import AddNoteForm from "./components/AddNoteForm";
import NoteList from "./components/NoteList";
import { useRootStore } from './store';

function App() {
  const { noteStore } = useRootStore();
  return (
    <div className="container">
      <AddNoteForm />
      <hr />
      <NoteList />
      <hr />
      <button onClick={() => noteStore.loadNotes()}>Load</button>
      <button onClick={() => noteStore.saveNotes()}>Save</button>
      <button onClick={() => noteStore.clear()}>Clear</button>
    </div>
  );
}

export default App;
