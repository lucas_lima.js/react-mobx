import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { makeAutoObservable } from 'mobx';

export class Timer {
  secondsPassed = 0;

  constructor() {
    makeAutoObservable(this);
  }

  increment() {
    this.secondsPassed += 1;
  }

  reset() {
    this.secondsPassed = 0;
  }
}

const timer = new Timer();

ReactDOM.render(
  <React.StrictMode>
    <App timer={timer} />
  </React.StrictMode>,
  document.getElementById('root')
);

setInterval(() => {
  timer.increment();
}, 1000);
