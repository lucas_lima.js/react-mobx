import React from "react";
import { observer } from "mobx-react";

import { Timer } from "./index";
import "./App.css";

interface AppProps {
  timer: Timer;
}

const App: React.FC<AppProps> = observer(({ timer }) => (
  <div className="container">
    <div className="timer">{timer.secondsPassed}s</div>
    <button onClick={() => timer.reset()}>Reset</button>
  </div>
));

export default App;
