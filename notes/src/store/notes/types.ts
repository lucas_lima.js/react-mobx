export interface NoteType {
  id: string;
  text: string;
}

export interface NoteStoreType {
  notes: NoteType[];
  addNote: (text: string) => void;
  removeNote: (id: string) => void;
  persist: () => void;
}
