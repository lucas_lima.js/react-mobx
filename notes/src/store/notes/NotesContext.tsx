import React from "react";
import { useLocalStore } from "mobx-react";

import { createNotesStore } from "./createNotesStore";
import { NoteStoreType } from "./types";

const NotesContext = React.createContext<NoteStoreType>({
  notes: [],
  addNote: () => {},
  removeNote: () => {},
  persist: () => {},
});

export const NotesProvider: React.FC = ({ children }) => {
  const notesStore = useLocalStore(createNotesStore);
  return (
    <NotesContext.Provider value={notesStore}>{children}</NotesContext.Provider>
  );
};

export const useNotesStore = () => React.useContext(NotesContext);
